#!/usr/bin/env python
"""
Created on Fri Jun 22 23:29:07 2018
Revised 2020-11-06

Features
- Cleanup existing .gz files from ImportReady folder
- Cleanup existing .txt files from ImportReady folder
- Cleanup existing .done files from ImportReady folder
- Unzip each .gz file in Inbound folder if there is a corresponding .done file
- If a .gz file exist without a corresponding .done file, log and skip it
- If the record count is not matching with what is on the file name, log and skip it
- If more than one .gz/.done pair exist for a single File/Staging table, merge them
- Processed file under Inbound folder is deleted
- Create a Process Summary file
- Log key events
Errors
- Exit with ErrorCode of 5 if there is record count mismatch with file name (Look into log file/ExternalTool log for more details)
- Exit with ErrorCode of 3 if at least one .done file missing (Look into log file/ExternalTool log for more details)
- Exit with ErrorCode of 1 for any unexpected errors (Look into log file/ExternalTool log for more details)

@author: NReddye, PMeyyappan
"""
from datetime import date
import time
import logging
import configparser
import os
import gzip
import shutil
import sys
import traceback
import getpass


# Initialize
def init():
    print("START-------------------------------------" + str(date.today()) + " " + time.strftime("%H:%M:%S"))
    print("Using " + os.getcwd() + "/" + sys.argv[1] + " on " + str(sys.version_info))
    #print(sys.path)
    print(getpass.getuser())
    set_global_variables()
    logging.debug(": Path:" + str(sys.path))
    print("INITIALIZED-------------------------------" + str(date.today()) + " " + time.strftime("%H:%M:%S"))


# Define Global Variables
def set_global_variables():
    global CONFIG
    global CONFIG_FILE
    global SLASH
    global LOG_FILE_DIR
    global LOG_FILE_PREFIX
    global INBOUND_FOLDER
    global ARCHIVE_FOLDER
    global IMPORT_READY_FOLDER
    global HAS_RECORD_COUNT_MISMATCH
    global IS_DONE_FILE_MISSING
    global FILES_SUMMARY
    global PROCESSING_SUMMARY_FILE

    HAS_RECORD_COUNT_MISMATCH = 'N'
    IS_DONE_FILE_MISSING = 'N'
    thisFolder = os.path.dirname(os.path.abspath(__file__))
    CONFIG_FILE = os.path.join(thisFolder, sys.argv[1])
    CONFIG = configparser.RawConfigParser()
    CONFIG.read(CONFIG_FILE, encoding=None)
    SLASH = CONFIG.get('EnvironmentSection', 'SLASH')
    LOG_FILE_DIR = CONFIG.get('InboundFileControllerSection', 'LOG_FILE_DIR')
    LOG_FILE_PREFIX = CONFIG.get('InboundFileControllerSection', 'LOG_FILE_PREFIX')
    INBOUND_FOLDER = CONFIG.get('InboundFileControllerSection', 'INBOUND_FOLDER')
    ARCHIVE_FOLDER = CONFIG.get('InboundFileControllerSection', 'ARCHIVE_FOLDER')
    print(ARCHIVE_FOLDER)
    if not (os.path.isdir(ARCHIVE_FOLDER)):
        os.mkdir(ARCHIVE_FOLDER)
    IMPORT_READY_FOLDER = CONFIG.get('InboundFileControllerSection', 'IMPORT_READY_FOLDER')
    PROCESSING_SUMMARY_FILE = CONFIG.get('InboundFileControllerSection', 'PROCESSING_SUMMARY_FILE')
    logLevel = CONFIG.get('EnvironmentSection', 'LOG_LEVEL')
    logFileName = LOG_FILE_DIR + SLASH + LOG_FILE_PREFIX + '_' + str(date.today()) + "_" + str(
        time.strftime("%H_%M_%S")) + '.log'
    logging.basicConfig(filename=logFileName,
                        filemode='a',
                        format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S',
                        level=logging.getLevelName(logLevel))
    # Contains list of files processed with
    # FileName, FileShortName, Inbound/Outbound, SysTodaysDate, ProcessingDate, ProcessingTime, RecCount, Status
    # to load sysFileControllerLog table
    FILES_SUMMARY = []
    time.sleep(1)
    logging.debug(": Log File Created")


# Merge 2 files
def mergefiles(targettxtfile, sourcetxtfile):
    tmpFile = open(targettxtfile + "_Tmp", 'wb')
    shutil.copyfileobj(open(targettxtfile, 'rb'), tmpFile)
    sourceFile = open(sourcetxtfile, 'rb')
    sourcetxt = sourceFile.readlines()
    for line in sourcetxt[1:]:
        tmpFile.write(line)
    sourceFile.close()
    tmpFile.close()
    os.rename(targettxtfile + "_Tmp", targettxtfile)
    os.remove(sourcetxtfile)


# Execute the core logic
def execute():
    global HAS_RECORD_COUNT_MISMATCH
    global IS_DONE_FILE_MISSING
    global FILES_SUMMARY
    # Cleanup old files from IMPORT_READY_FOLDER
    importreadyList_gz = [x for x in os.listdir(IMPORT_READY_FOLDER) if x.endswith(".gz")]
    for x in importreadyList_gz:
        logging.info(": Deleting previous File " + x)
        os.remove(IMPORT_READY_FOLDER + SLASH + x)

    importreadyList_txt = [x for x in os.listdir(IMPORT_READY_FOLDER) if x.endswith(".txt")]
    for x in importreadyList_txt:
        logging.info(": Deleting previous File " + x)
        os.remove(IMPORT_READY_FOLDER + SLASH + x)

    importreadyList_done = [x for x in os.listdir(IMPORT_READY_FOLDER) if x.endswith(".done")]
    for x in importreadyList_done:
        logging.info(": Deleting previous File " + x)
        os.remove(IMPORT_READY_FOLDER + SLASH + x)

    print("CurrentFiles:")
    gZList = [x for x in os.listdir(INBOUND_FOLDER) if x.endswith(".gz")]
    print(gZList)

    doneList = [x for x in os.listdir(INBOUND_FOLDER) if x.endswith(".done")]
    print(doneList)


    # Process each file set
    for x in gZList:

        # Prepare file names
        logging.info(": Processing file  : " + x)
        sourceSystem = x[:x.find('~')]
        withoutSourceSystem = x[x.find('~') + 1:]
        fileshortname = withoutSourceSystem[:withoutSourceSystem.find('~')]
        filename = fileshortname + '.txt'
        logging.debug(
            ": sourceSystem:" + sourceSystem + "_withoutSourceSystem:" + withoutSourceSystem + "_filename:" + filename)
        donefile = x + '.done'
        inbound_filenameWithPath = INBOUND_FOLDER + SLASH + x
        logging.debug(": inbound_filenameWithPath:" + inbound_filenameWithPath)
        importready_filename = IMPORT_READY_FOLDER + SLASH + filename
        importready_filenameWithPath = IMPORT_READY_FOLDER + SLASH + x
        txtFilenameWithPath = importready_filenameWithPath[:-3]
        logging.debug(": txtFilenameWithPath:" + txtFilenameWithPath)
        doneFilenameWithPath = INBOUND_FOLDER + SLASH + x + '.done'
        logging.debug(": doneFilenameWithPath:" + doneFilenameWithPath)
        recCntFromFileName = x.split('~')[len(x.split('~')) - 1].split('.')[0]

        # If .done file is available
        if os.path.isfile(doneFilenameWithPath):
            logging.debug(": done file exists " + doneFilenameWithPath)
            shutil.copy(inbound_filenameWithPath, ARCHIVE_FOLDER)
            shutil.copy(inbound_filenameWithPath, IMPORT_READY_FOLDER)
            with open(txtFilenameWithPath, 'wb') as f_out, gzip.open(inbound_filenameWithPath, 'rb') as f_in:
                shutil.copyfileobj(f_in, f_out)
                f_in.close()
                f_out.close()
            recCnt = sum(1 for line in open(txtFilenameWithPath)) - 1

            # Merge if another more than one file is processed for same table load
            if os.path.isfile(importready_filename) and recCnt == recCntFromFileName:
                logging.info(": Merging file : " + txtFilenameWithPath + " with " + importready_filename)
                mergefiles(importready_filename, txtFilenameWithPath)
                FILES_SUMMARY.append([x, fileshortname, "INBOUND", '', date.today(), time.strftime("%H:%M:%S"), recCnt, "PROCESSED"])
            # Name file appropriately for import
            elif recCnt == int(recCntFromFileName):
                os.rename(txtFilenameWithPath, importready_filename)
                FILES_SUMMARY.append([x, fileshortname, "INBOUND", '', date.today(), time.strftime("%H:%M:%S"), recCnt, "PROCESSED"])
            # If record count mismatch with that in file name, skip the load
            else:
                HAS_RECORD_COUNT_MISMATCH = 'Y'
                FILES_SUMMARY.append([x, fileshortname, "INBOUND", '', date.today(), time.strftime("%H:%M:%S"), recCnt, "ERROR-CNT"])
                print(": Skipping .gz file as record count doesnt match for : " + inbound_filenameWithPath)
                logging.error(": Skipping .gz file as record count doesnt match for : " + inbound_filenameWithPath)
                os.remove(txtFilenameWithPath)
            logging.info(": Deleting File and .done for: " + inbound_filenameWithPath)
            os.remove(inbound_filenameWithPath)
            os.remove(doneFilenameWithPath)
            logging.info(": Unzipped file : " + x)

        # If .done file is unavailable
        else:
            IS_DONE_FILE_MISSING = 'Y'
            FILES_SUMMARY.append([x, fileshortname, "INBOUND", '', date.today(), time.strftime("%H:%M:%S"), recCnt, "ERROR-.done"])
            logging.error(": Skipping .gz file as .done file does not exist for : " + inbound_filenameWithPath)



try:
    errorCode = 0
    init()
    execute()
    print("DONE-------------------------------------" + str(date.today()) + " " + time.strftime("%H:%M:%S"))
    if IS_DONE_FILE_MISSING == 'Y':
        errorCode = 3
    if HAS_RECORD_COUNT_MISMATCH == 'Y':
        errorCode = 5
except Exception as e:
    print(e)
    logging.error(traceback.format_exc())
    errorCode = 1
finally:
    # Overwrite ProcessingSummary log file
    with open(PROCESSING_SUMMARY_FILE, 'w') as f_out:
        f_out.write(
            "FileName" + "\t" + "FileShortName" + "\t" + "Type" + "\t" + "SysTodaysDate" + "\t" + "ProcessingDate" + "\t" +
            "ProcessingTime" + "\t" + "RecCount" + "\t" + "Status" + "\n")
        for listitem in FILES_SUMMARY:
            for column in listitem:
                f_out.write('%s\t' % column)
            f_out.write('\n')
        f_out.close()
    if errorCode == 0:
        logging.info(": Inbound File Controller steps Complete")
    else:
        sys.exit(errorCode)

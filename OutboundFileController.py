#!/usr/bin/env python
"""
Created 2020-11-06

Features
- Cleanup old files in Outbound folders those start with a configured prefix under a configured parent
- Process files that start with a configured prefix under Publication folder and all of its subfolders
- Special handling of Payroll prefixed files to rename headers with special characters and no compression
- Other files are gzipped and a corresponding .done file is created
- Processed output is moved to the corresponding Outbound folder and archive
- Processed file under Publication folder is deleted
- Create a Process Summary file
- Log key events
Errors
- Exit with ErrorCode of 1 for any unexpected errors (Look into log file/ExternalTool log for more details)


@author: PMeyyappan
"""
from datetime import date
import time
import logging
import configparser
import os
import gzip
import shutil
import sys
import traceback
import getpass


# Initialize
def init():
    print("START-------------------------------------" + str(date.today()) + " " + time.strftime("%H:%M:%S"))
    print("Arr:" + str(len(sys.argv)) + ":" + str(sys.argv))
    print("Using " + os.getcwd() + "/" + sys.argv[1] + " on " + str(sys.version_info))
    # print(sys.path)
    print(getpass.getuser())
    set_global_variables()
    logging.debug(": Path:" + str(sys.path))
    print("INITIALIZED-------------------------------" + str(date.today()) + " " + time.strftime("%H:%M:%S"))


# Define Global Variables
def set_global_variables():
    global CONFIG
    global CONFIG_FILE
    global SLASH
    global LOG_FILE_DIR
    global LOG_FILE_PREFIX
    global PUBLICATION_FOLDER
    global PUBLICATION_FOLDER_FULL_SCAN
    global PUBLISHED_FILES_PREFIX
    global OUTBOUND_FOLDER_PARENT
    global OUTBOUND_FOLDER_PREFIX
    global DELETE_OLD_FILES
    global ARCHIVE_FOLDER
    global PAYROLL_FILE_PREFIXES
    global COMPLETE_FILE_EXTN
    global COMPLETION_FLAG_FILE
    global FILES_SUMMARY
    global PROCESSING_SUMMARY_FILE

    thisFolder = os.path.dirname(os.path.abspath(__file__))
    CONFIG_FILE = os.path.join(thisFolder, sys.argv[1])
    CONFIG = configparser.RawConfigParser()
    CONFIG.read(CONFIG_FILE, encoding=None)
    SLASH = CONFIG.get('EnvironmentSection', 'SLASH')
    LOG_FILE_DIR = CONFIG.get('OutboundFileControllerSection', 'LOG_FILE_DIR')
    LOG_FILE_PREFIX = CONFIG.get('OutboundFileControllerSection', 'LOG_FILE_PREFIX')
    PUBLICATION_FOLDER = CONFIG.get('OutboundFileControllerSection', 'PUBLICATION_FOLDER')
    PUBLICATION_FOLDER_FULL_SCAN = CONFIG.get('OutboundFileControllerSection', 'PUBLICATION_FOLDER_FULL_SCAN')
    PUBLISHED_FILES_PREFIX = CONFIG.get('OutboundFileControllerSection', 'PUBLISHED_FILES_PREFIX')
    OUTBOUND_FOLDER_PARENT = CONFIG.get('OutboundFileControllerSection', 'OUTBOUND_FOLDER_PARENT')
    OUTBOUND_FOLDER_PREFIX = CONFIG.get('OutboundFileControllerSection', 'OUTBOUND_FOLDER_PREFIX')
    DELETE_OLD_FILES = CONFIG.get('OutboundFileControllerSection', 'DELETE_OLD_FILES')
    ARCHIVE_FOLDER = CONFIG.get('OutboundFileControllerSection', 'ARCHIVE_FOLDER')
    if not (os.path.isdir(ARCHIVE_FOLDER)):
        os.mkdir(ARCHIVE_FOLDER)
    PAYROLL_FILE_PREFIXES = CONFIG.get('OutboundFileControllerSection', 'PAYROLL_FILE_PREFIXES')
    COMPLETE_FILE_EXTN = CONFIG.get('OutboundFileControllerSection', 'COMPLETE_FILE_EXTN')
    COMPLETION_FLAG_FILE = CONFIG.get('OutboundFileControllerSection', 'COMPLETION_FLAG_FILE')
    PROCESSING_SUMMARY_FILE = CONFIG.get('InboundFileControllerSection', 'PROCESSING_SUMMARY_FILE')
    logLevel = CONFIG.get('EnvironmentSection', 'LOG_LEVEL')
    logFileName = LOG_FILE_DIR + SLASH + LOG_FILE_PREFIX + '_' + str(date.today()) + "_" + str(
        time.strftime("%H_%M_%S")) + '.log'
    logging.basicConfig(filename=logFileName,
                        filemode='a',
                        format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S',
                        level=logging.getLevelName(logLevel))
    time.sleep(1)
    # Contains list of files processed with
    # FileName, FileShortName, Inbound/Outbound, SysTodaysDate, ProcessingDate, ProcessingTime, RecCount, Status
    # to load sysFileControllerLog table
    FILES_SUMMARY = []
    logging.debug(": Log File Created")


# Merge 2 files
def mergefiles(targettxtfile, sourcetxtfile):
    tmpFile = open(targettxtfile + "_Tmp", 'wb')
    shutil.copyfileobj(open(targettxtfile, 'rb'), tmpFile)
    sourceFile = open(sourcetxtfile, 'rb')
    sourcetxt = sourceFile.readlines()
    for line in sourcetxt[1:]:
        tmpFile.write(line)
    sourceFile.close()
    tmpFile.close()
    os.rename(targettxtfile + "_Tmp", targettxtfile)
    os.remove(sourcetxtfile)


# Execute the core logic
def execute():
    global FILES_SUMMARY
    # Cleanup old files from OUTBOUND_FOLDER
    if DELETE_OLD_FILES == 'Y':
        for directoryP in os.listdir(OUTBOUND_FOLDER_PARENT):
            if OUTBOUND_FOLDER_PREFIX == "":
                for root, directories, filenames in os.walk(OUTBOUND_FOLDER_PARENT):
                    for filename in filenames:
                        logging.info(": Deleting previous File " + filename)
                        print(": Deleting previous File " + filename)
                        os.remove(root + SLASH + filename)
            elif directoryP.find(OUTBOUND_FOLDER_PREFIX) == 0:
                for root, directories, filenames in os.walk(os.path.join(OUTBOUND_FOLDER_PARENT, directoryP)):
                    for filename in filenames:
                        logging.info(": Deleting previous File " + filename)
                        print(": Deleting previous File " + filename)
                        os.remove(root + SLASH + filename)

    print("CurrentFiles:")
    logging.info(": CurrentFiles:")
    pubList = []
    if PUBLICATION_FOLDER_FULL_SCAN == 'Y':
        for root, directories, filenames in os.walk(PUBLICATION_FOLDER):
            for directory in directories:
                print("Processing Directory : " + os.path.join(root, directory))
            for filename in filenames:
                if (filename.find("~-") != -1 and filename.find(
                        PUBLISHED_FILES_PREFIX) == 0) and not (filename.endswith(".gz")):
                    subDir = root.split(SLASH)[len(root.split(SLASH)) - 1]
                    if subDir == PUBLICATION_FOLDER.split(SLASH)[len(PUBLICATION_FOLDER.split(SLASH)) - 1]:
                        subDir = ""
                    pubList.append([root, subDir, filename])
    else:
        print("Else")
        for filename in os.listdir(PUBLICATION_FOLDER):
            if os.path.isfile(PUBLICATION_FOLDER + SLASH + filename) and filename.find("~-") != -1 and filename.find(
                    PUBLISHED_FILES_PREFIX) == 0 and not (filename.endswith(".gz")):
                pubList.append([PUBLICATION_FOLDER, "", filename])

    print(pubList)
    logging.info(pubList)

    # Process each file set
    for x in pubList:
        isFileProcessed = 0
        # Get record count excluding header
        recCnt = sum(1 for line in open(x[0] + SLASH + x[2])) - 1
        # Build new file name
        timestmp = x[2].split(".")[0].split("~-")[1].split("-")
        newFileName = x[2].split("~-")[0] + '~' + timestmp[0] + timestmp[1] + timestmp[2] + '-' + timestmp[3][0:6] + '~' + str(recCnt) + '.' + \
                      x[2].split(".")[1]
        fileShortName = x[2].split("~-")[0].split("~")[1]

        logging.info(": Processing file  : " + x[2] + " : Record Count : " + str(recCnt))

        # Process files with Payroll prefixes
        for payrollFilePrefix in PAYROLL_FILE_PREFIXES.split(','):
            if isFileProcessed == 0 and x[2].split("~-")[0] == payrollFilePrefix:
                with open(x[0] + SLASH + x[2]) as f_in:
                    lines = f_in.readlines()
                    f_in.close()
                # Replace headers as needed
                lines[0] = lines[0].replace("File_Number", "File #").replace("Pay_Number", "Pay #").replace("?",
                                                                                                            "").replace(
                    "FileNumber", "File #").replace("File Number", "File #").replace("Pay Number", "Pay #")
                with open(x[0] + SLASH + x[2], "w") as f_out:
                    f_out.writelines(lines)
                f_out.close()
                isFileProcessed = 1


                with open(OUTBOUND_FOLDER_PARENT + SLASH + OUTBOUND_FOLDER_PREFIX + x[
                1] + SLASH + newFileName + '.' + COMPLETE_FILE_EXTN, "w") as f_out:
                    pass
                f_out.close()
                isFileProcessed = 1



        # Process files without Payroll prefixes
        if isFileProcessed == 0:
            
            
            newFileName = newFileName + ".gz"
            # Compress
            
            with open(x[0] + SLASH + x[2], 'rb') as f_in, gzip.open(x[0] + SLASH + newFileName,
                                                                    'wb') as f_out:
                f_out.writelines(f_in)
            f_in.close()
            f_out.close()
            os.remove(x[0] + SLASH + x[2])
            x[2] = newFileName
            # Create .done file
            with open(OUTBOUND_FOLDER_PARENT + SLASH + OUTBOUND_FOLDER_PREFIX + x[
                1] + SLASH + newFileName + '.' + COMPLETE_FILE_EXTN, "w") as f_out:
                pass
            f_out.close()

        FILES_SUMMARY.append(
            [x[2], fileShortName, "OUTBOUND", '', date.today(), time.strftime("%H:%M:%S"), recCnt, "PROCESSED"])

        # Create overall .done file
        with open(OUTBOUND_FOLDER_PARENT + SLASH + OUTBOUND_FOLDER_PREFIX + SLASH + COMPLETION_FLAG_FILE, "w") as f_out:
            pass
        f_out.close()

        # Copy files to Outbound and Archive folders
        shutil.copyfileobj(open(x[0] + SLASH + x[2], 'rb'),
                           open(OUTBOUND_FOLDER_PARENT + SLASH + OUTBOUND_FOLDER_PREFIX + x[1] + SLASH + newFileName,
                                'wb'))
        shutil.copyfileobj(open(x[0] + SLASH + x[2], 'rb'), open(ARCHIVE_FOLDER + SLASH + newFileName, 'wb'))

        # Remove file from Publication folder
        os.remove(x[0] + SLASH + x[2])


try:
    errorCode = 0
    init()
    execute()
    print("DONE-------------------------------------" + str(date.today()) + " " + time.strftime("%H:%M:%S"))
except Exception as e:
    print(traceback.format_exc())
    logging.error(traceback.format_exc())
    errorCode = 1
finally:
    # Overwrite ProcessingSummary log file
    with open(PROCESSING_SUMMARY_FILE, 'w') as f_out:
        f_out.write(
            "FileName" + "\t" + "FileShortName" + "\t" + "Type" + "\t" + "SysTodaysDate" + "\t" + "ProcessingDate" + "\t" +
            "ProcessingTime" + "\t" + "RecCount" + "\t" + "Status" + "\n")
        for listitem in FILES_SUMMARY:
            for column in listitem:
                f_out.write('%s\t' % column)
            f_out.write('\n')
        f_out.close()
    logging.info(": Outbound File Controller steps Complete")
    if errorCode == 0:
        logging.info(": Inbound File Controller steps Complete")
    else:
        sys.exit(errorCode)
